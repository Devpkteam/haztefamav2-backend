const Membresia = require('../models/membresia');
var Bitacora = require('../models/bitacora')
const historialPagos = require('../models/historialPagos')
const solicitudMembresias = require('../models/solicitudMembresias')
const axios = require('axios')
const io = require('../socket');
const membresiaCtrl = {};
var ipn = require('paypal-ipn');
const { param } = require('../routes/membresias.routes');
const usuario = require('../models/usuario');

membresiaCtrl.getALL = async (req,res) => {
    Membresia.find({borrado:false})
        .exec((err,membresias)=>{
            if(err){
                res.status(200).json({
                    ok:false,
                    err
                })
            }else{
                res.status(200).json({
                    ok:true,
                    membresias
                })
            }
        })
}

membresiaCtrl.newMembresia = async (req,res) => {
    let membresia = new Membresia(req.body)
    membresia.save((err,membresia)=>{
        if(err){
            res.status(200).json({
                ok:false,
                err
            })
        }else{
            Bitacora.create({
                usuario:req.body.idAdmin,
                accion:`Creo la membresia ${membresia.nombre}`
            })
            res.status(200).json({
                ok:true,
                membresia
            })
        }
    })
}

membresiaCtrl.updateMembresia = async (req,res) => {
   Membresia.findByIdAndUpdate(req.params.id,req.body)
        .exec((err,membresia)=>{
            if(err){
                res.status(200).json({
                    ok:false,
                    err
                })
            }else{
                Bitacora.create({
                    usuario:req.body.idAdmin,
                    accion:`Edito la membresia ${membresia.nombre}`
                })
                res.status(200).json({
                    ok:true,
                    membresia
                })
            }
        })
}


membresiaCtrl.updateStatusById = (req,res) => {
    Membresia.findById(req.body.id)
    .exec((err,membresia)=>{
        if(err){
            res.status(200).json({
                ok:false,
                msg:`Hubo un error al intentar cambiar el estado a la membresia`,
                err:err
            })
        }else{
            
            Membresia.findByIdAndUpdate(membresia._id,{status:!membresia.status,fecha_modificacion: Date.now()})
            .exec((err,membresia)=>{
                if(err){
                    res.status(200).json({
                        ok:false,
                        msg:`Hubo un error al intentar cambiar el estado a la membresia`,
                        err:err
                    })
                }else{
                    Bitacora.create({
                        usuario:req.body.idAdmin,
                        accion:`Cambio el estado de la membresia ${membresia.nombre} a '${!membresia.status?'Activa':'Desactivada'}'`
                    })
                    res.status(200).json({
                        ok:true,
                        msg:'membresia modificada',
                        membresia
                    })
                }
            })
        }
    })
   
}


membresiaCtrl.deleteById =  (req,res)=>{

  
    Membresia.findByIdAndUpdate(req.params.id,{borrado:true,fecha_modificacion:Date.now()})
    .exec((err,membresia)=>{
        if(err){
            res.status(200).json({
                ok:false,
                msg:`Hubo un error al intentar eliminar la Membresia con  el id = ${req.params.id}`,
                err:err
            })
        }else{
            Bitacora.create({
                usuario:req.params.idAdmin,
                accion:`Borro la membresia ${membresia.nombre}`
            })
            res.status(200).json({
                ok:true,
                msg:'membresia eliminada',
                membresia
            })
        }
    })


}


function randomString(length, chars) {
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
}


function actualizarCreditoBitacora( email, creditos ) {
    return new Promise((resolve, reject) => {

        usuario.find( { email })
        .populate('usuarios', 'nombre email')
        .exec( (err, usuarioDB) => {
            creditosActual = parseInt(usuarioDB[0].creditos);
            creditoEntrante = parseInt(creditos);
            let total = creditosActual + creditoEntrante;
            console.log(usuarioDB[0]._id);
            usuario.updateOne({"email":params.payer_email},{$set:{"creditos":total}}).then  (err, results => {});
            Bitacora.create({
                usuario:usuarioDB[0]._id,
                accion:`Realizó una compra exitosa de ${creditos} créditos`
            }).then((err, results => {console.log(err, results)}))      
        });
    })
}

function crearBitacora( email ) {
    usuario.find( { email })
    .populate('usuarios', 'nombre email')
    .exec( (err, usuarioDB) => {
        
    });
}

function getMembresia ( monto ) {
    return new Promise((resolve, reject) => {
        Membresia.find({borrado:false})
        .exec((err,membresias)=>{
            let creditos = 0;
            for ( let i of membresias  ){
                console.log(i.precio);
                if ( i.precio === parseInt(monto)  ){
                    creditos = i.creditos; 
                }
            }
            resolve(creditos)
            console.log(creditos);
        })
    });
}

membresiaCtrl.comprarMembresia =  (req,res) =>{
    res.send(200);
    ipn.verify(req.body,{'allow_sandbox': true}, function callback(err, msg) {
        console.log(msg);
        if (err) {
          console.error(err);
        } else {
            params=req.body;
            console.log(req.body);
            if (params.payment_status == 'Completed') {
                getMembresia( params.mc_gross )
                .then(creditos => {
                    actualizarCreditoBitacora(params.payer_email, creditos)
                });
            }
        }
      });



    // req.body.order = randomString(10,'1234567890')

    // let solicitud = new solicitudMembresias(req.body)

    // ;(await solicitud.save()).populate('usuario membresia',(err,solicitud)=>{
        

    //     axios.post('https://des.payku.cl/api/transaction', {
    //         email:`${solicitud.usuario.email}`,
    //         order:`${solicitud.order}`,
    //         subject:`Comprar membresia ${solicitud.membresia.nombre}`,
    //         amount:solicitud.membresia.precio,
    //         payment:99,
    //         urlnotify:"http://64.227.31.7:3000/pagos/pagomembresia"
        
        
    //     }
    //       ,{headers:{
    //         'Content-Type': 'application/json',
    //         'Authorization': 'Bearer a4f6d118b3a5a0213f46461ad587b712'
    //     }})
    //     .then((resp) => {
    //         console.log(resp.data);
    //         Bitacora.create({
    //             usuario:solicitud.usuario._id,
    //             accion:`Hizo una solicitud de compra de la membresia ${solicitud.membresia.nombre}`
    //         })

    //         historialPagos.create({
    //             usuario:solicitud.usuario._id,
    //             accion:`Solicito comprar la membresia ${solicitud.membresia.nombre}`,
    //             monto:solicitud.membresia.precio
    //         })
    //         io.to('administradores').emit('Pagos','Pago actualizado')
            
            
    //         res.status(200).json({
    //             ok:true,
    //             data:resp.data
    //         })
           
           
          
    //     })
    //     .catch((err) => {
    //         console.log('hubo un error tambien',err);
            
    //         res.status(200).json({
    //             ok:false,
    //             err
    //         })
    //     })
        
    // })
      
}

module.exports = membresiaCtrl;
