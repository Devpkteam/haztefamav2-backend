const Bitacora = require('../models/bitacora');
const Usuario = require('../models/usuario');
const Pagos = require('../models/pagos');

const historialPagos = require('../models/historialPagos')
const solicitudMembresias = require('../models/solicitudMembresias')
const fs = require('fs');
const mongoXlsx = require('mongo-xlsx');
const usersSocket = require('../userSocket')
const io = require('../socket');


const axios = require('axios');
const solicitudPagoProveedor = require('../models/solicitudPagoProveedor');
const Cotizacion = require('../models/cotizacion');
const pagosCtrl = {};


pagosCtrl.historial = async (req,res) => {
    historialPagos.find()
        .populate('usuario')
        .sort({fecha_creacion:-1})
        .exec((err,historial)=>{
            if(err){
                res.status(200).json({
                    ok:false,
                    err
                })
            }else{
                res.status(200).json({
                    ok:true,
                    historial
                })
            }
        })
}

pagosCtrl.historialByUser = async (req,res) => {
    historialPagos.find({usuario:req.params.id})
        .populate('usuario')
        .sort({fecha_creacion:-1})
        .exec((err,historial)=>{
            if(err){
                res.status(200).json({
                    ok:false,
                    err
                })
            }else{
                res.status(200).json({
                    ok:true,
                    historial
                })
            }
        })
}
pagosCtrl.historialById = async (req,res) => {
    historialPagos.find({_id:req.params.id})
        .populate('usuario')
        .sort({fecha_creacion:-1})
        .exec((err,historial)=>{
            if(err){
                res.status(200).json({
                    ok:false,
                    err
                })
            }else{
                res.status(200).json({
                    ok:true,
                    historial,
                    url:`${req.protocol}://${req.headers.host}/user/uploads/`
                })
            }
        })
}
pagosCtrl.toExcel = async (req,res) => {
    let data = [];
    let condicions
    condicions = {
        fecha_creacion: {
            $gte: req.body.since,
            $lte: req.body.until
        }
    }
    historialPagos.find()
        .populate('usuario')
        .exec((err, historial) => {
            if(err){
                res.status(200).json({
                    ok: false,
                    err
                })
            } else {
                historial.map(x =>{

                    let f = new Date(x.fecha_creacion)
                    let d = f.toString() 
                    let data1 = {
                        Nombre : x.usuario.nombre,
                        Apellido: x.usuario.apellido,
                        Username: x.usuario.username,
                        Email: x.usuario.email,
                        Accion: x.accion,
                        Fecha: d,
                    }
                    data.push(data1)
                })
                console.log(data);
                
                let model = mongoXlsx.buildDynamicModel(data)
        
                mongoXlsx.mongoData2Xlsx(data, model, function(err, data) {
                    
                    let dirname = __dirname.replace('controller', data.fullPath)                    
                    console.log(__dirname); 
                    res.status(200).sendFile(dirname, (err)=>{
                        if (err) {
                            console.log(err);
                        } 
                    })


                });
                
            }
        })
}

pagosCtrl.pagoMembresia = async (req,res)=>{
    // esta es la respuesta de payku

    console.log('aqui estoy')

    // validamos que la compra se realizo correctamente
    axios.post('https://des.payku.cl/api/verificar', req.body
      ,{headers:{
        'Content-Type': 'application/json',
        'Authorization': 'Bearer a4f6d118b3a5a0213f46461ad587b712'
    }})
    .then((resp) => {
        // si todo sale bien traera esta respuesta 

        // Buscamos las solicitudes por el numero de order 
        solicitudMembresias.findOne({order:req.body.order})
            .populate('usuario membresia')
            .exec((err,solicitud)=>{

                
                if(resp.data.status == 'VALID'){
                    // Ya teniendo la solicitud, validamos que la respuesta de payku sea valida 

                    resp.data.transaction.usuario = solicitud.usuario._id // le asignamos el id del usuario de la solicitud a la transaccion
                    Pagos.create(resp.data.transaction,(err,pago)=>{
                        // Aqui registramos todos los datos del pago, por si lo necesitamos luego
                        if(err){
                    
                         
                        }else{          
                            // si no hay error procedemos a guardar en el historial de pagos, que la compra se concreto
                            historialPagos.create({
                                usuario:solicitud.usuario._id,
                                accion:`Concreto la compra de la membresia ${solicitud.membresia.nombre}`,
                                monto:solicitud.membresia.precio
                            })
                            io.to('administradores').emit('Pagos','Pago actualizado')
                            // Se procede a sumarle los creditos al usuario que realizo la compra 
                            Usuario.findByIdAndUpdate(solicitud.usuario._id,{$inc: {creditos: solicitud.membresia.creditos }})
                                .exec((err,userCredits)=>{
                                    if(err){
                                   
                                        
                                    }else{
                                        // si no hay errores, guardamos en la bitacora que el usuario concreto la compra 
                                        Bitacora.create({
                                            usuario:userCredits._id,
                                            accion:`Concreto la compra de la membresia ${solicitud.membresia.nombre}`
                                        })

                                        // como esta respuesta es a la api de payku, utilizaremos sockets para comunicarnos con haztefama
                                        usersSocket.getUserByIdUser(solicitud.usuario._id).then((usuario)=>{
                                          
                                            
                                            io.to(usuario.id).emit('pago_success',{msg:'Su pago se registro correctamente'})
                                        })

                                        
                                    }


                                                         
                                })                             
                          
                        }
                        
                    })
                }else{
                    usersSocket.getUserByIdUser(solicitud.usuario._id).then((usuario)=>{
                     
                        io.to(usuario.id).emit('err_pago',{msg:'Hubo un error al intentar procesar su pago, por favor verifique'})
                    })
              
                }
                


            })
      
      
    })
    .catch((error) => {
     
    })
    res.send(true)
}

pagosCtrl.pagoProveedor = async (req,res)=>{
    // esta es la respuesta de payku

    console.log('aqui estoy');

    // validamos que la compra se realizo correctamente
    axios.post('https://des.payku.cl/api/verificar', req.body
      ,{headers:{
        'Content-Type': 'application/json',
        'Authorization': 'Bearer a4f6d118b3a5a0213f46461ad587b712'
    }})
    .then((resp) => {
        // si todo sale bien traera esta respuesta 

        // Buscamos las solicitudes por el numero de order 
        solicitudPagoProveedor.findOne({order:req.body.order})
            .populate('usuario servicios cotizaciones')
            .exec((err,solicitud)=>{

                
                if(resp.data.status == 'VALID'){
                    // Ya teniendo la solicitud, validamos que la respuesta de payku sea valida 

                    resp.data.transaction.usuario = solicitud.usuario._id // le asignamos el id del usuario de la solicitud a la transaccion
                    Pagos.create(resp.data.transaction,(err,pago)=>{
                        // Aqui registramos todos los datos del pago, por si lo necesitamos luego
                        if(err){
                    
                         
                        }else{          
                            // si no hay error procedemos a guardar en el historial de pagos, que la compra se concreto
                            historialPagos.create({
                                usuario:solicitud.usuario._id,
                                accion:`Concreto Pago de servicio ${solicitud.membresia.nombre}`,
                                monto:solicitud.cotizaciones.monto
                            })
                            io.to('administradores').emit('Pagos','Pago actualizado')
                            // Se procede a sumarle los creditos al usuario que realizo la compra 

                            if (solicitud.cotizaciones.monto == solicitud.montoPagado && solicitud.porcentajeCancelado == 100) {
                                status = 3
                            } else {
                                status = 2
                            }

                            Cotizacion.findByIdAndUpdate(req.body.cotizaciones, {status: status, porcentajeCancelado: solicitid.porcentajeCancelado, montoPagado: solicitud.montoPagado})
                            .exec((err, cotizacionUpdate) => {
                                if(err){
                                    res.status(200).json({
                                        ok: false,
                                        msg: `hubo un error al intentar guardar con nombre = ${cotizacionUpdate.nombre}`,
                                        err: err
                                    })
                                }else{
                                    // si no hay errores, guardamos en la bitacora que el usuario concreto la compra 
                                    Bitacora.create({
                                        usuario: cotizacionUpdate._id,
                                        accion:`Concreto la compra de la membresia ${solicitud.membresia.nombre}`
                                    })

                                    // como esta respuesta es a la api de payku, utilizaremos sockets para comunicarnos con haztefama
                                    usersSocket.getUserByIdUser(solicitud.usuario._id).then((usuario)=>{
                                      
                                        
                                        io.to(usuario.id).emit('pago_success',{msg:'Su pago se registro correctamente'})
                                    })

                                    
                                }
                            })               
                          
                        }
                        
                    })
                }else{
                    usersSocket.getUserByIdUser(solicitud.usuario._id).then((usuario)=>{
                     
                        io.to(usuario.id).emit('err_pago',{msg:'Hubo un error al intentar procesar su pago, por favor verifique'})
                    })
              
                }
                


            })
      
      
    })
    .catch((error) => {
     
    })
    res.send(true)
}

pagosCtrl.pagar = async (req,res)=>{
    console.log(req);
    var pagos = new Pagos({
        usuario: req.body.purchase_units[0].reference_id,
        datosPagos: req.body
    })
    console.log(pagos);

    pagos.save( ).then( 
        (pagoGuardado) => {                
            res.status(201).json({
                ok: true,
                pago: pagoGuardado
            });
        }
    ).catch((err) => {
        return res.status(500).json({
            ok: false,
            msj: 'Error al pagar',
            errors: err
        });
    }) 
}

pagosCtrl.balance = async (req,res)=>{
    Usuario.find({  }, 'creditos')
    .exec(
        ( err, balance ) => {
            if( err ) {
                return res.status(200).json({
                    ok: false,
                    msj: 'Error cargando datos',
                    errors: err
                });
            } 

            res.status(200).json({
                ok: true,
                balance,
            });
    });
};

module.exports = pagosCtrl