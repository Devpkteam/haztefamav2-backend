const Bitacora = require('../models/bitacora');
const ChatPostulacion = require('../models/chatPostulaciones')
const Notificaciones = require('../models/notificaciones')
const Postulaciones = require('../models/postulaciones');
const Usuario = require('../models/usuario');
const mensajesPostulacionCtrl = {};
const usersSocket = require("../userSocket");
const Mailer = require("../src/emails/emails.module");
const io = require("../socket");
const bqConfig = require('../src/config/configurations');
io.on("connect", (socket) => {
    console.log("socket connect");
})
mensajesPostulacionCtrl.getMensajesByPostulacionId = async (req, res) => {

    ChatPostulacion.find({ postulacion: req.params.id })
        .sort({ fecha_creacion: 1 })
        .populate('usuario postulacion')
        .exec((err, chat) => {
            if (err) {
                res.status(200).json({
                    ok: false,
                    err
                })
            } else {
                res.status(200).json({
                    ok: true,
                    chat,
                    url: `${req.protocol}://${req.headers.host}/user/uploads/`,
                    urlDownload: `${req.protocol}://${req.headers.host}/user/documentuploads/`,
                })
            }
        })

}

mensajesPostulacionCtrl.nuevoMensajeByPosutulacionId = async (req, res) => {

    let mensaje = new ChatPostulacion(req.body)
    mensaje.save((err, mensaje) => {
        if (err) {
            res.status(200).json({
                ok: false,
                err
            })
        } else {
            console.log("guardar mensaje en BD de notificaciones");
            _generateAlert(req.body);
            res.status(200).json({
                ok: true,
                mensaje
            })
        }
    })
}
const _generateAlert = async (body) => {
    console.log(body);
    await Postulaciones.findOne({ _id: body.postulacion }).populate("servicio").populate("usuario").then(async (postulacion) => {
        console.log(postulacion.servicio.usuario);
        console.log(body.usuario);
        if (body.usuario.toString() == postulacion.servicio.usuario.toString()) {
            console.log("mensaje de cliente a proveedor");
            var link = "proveedor/requerimientos/chat";
            var link1 = "#/proveedor/requerimientos/chat";
            const notificacion = new Notificaciones(
                {
                    usuario: postulacion.usuario._id,
                    mensaje: 'Nueva mensaje en servicio',
                    link: link,
                    parametros: { id: body.postulacion }
                }
            )
            notificacion.save().then((x)=>{
                console.info("notificacion guardada");
                io.to( postulacion.usuario._id).emit("nueva_notificacion_r", {
                    msg: "Nueva notificacion",
                });
                io.to(body.postulacion).emit("nuevo_mensaje_r", {
                    msg: "Nuevo mensaje",
                });
            })
            let creator;
            link1 = link1 + "?id="+body.postulacion;
            await Usuario.findOne({ _id: postulacion.servicio.usuario }).then((user) => {
                creator = user;
            })
            _saveMail(postulacion.usuario, creator , link1 , postulacion.servicio)
        } else {
            console.log("mensaje de proveedor a cliente")
            let creator;
            await Usuario.findOne({ _id: postulacion.servicio.usuario }).then((user) => {
                console.log("User")
                // console.log(user)
                var link = "/cliente/servicio/postulaciones/chat";
                var link1 = "#/cliente/servicio/postulaciones/chat";
                const notificacion = new Notificaciones(
                    {
                        usuario: user._id,
                        mensaje: 'Nueva mensaje en servicio',
                        link: link,
                        parametros: { id: body.postulacion }
                    }
                )
                link1 = link1 + "?id="+body.postulacion;
                notificacion.save().then((x)=>{
                    console.info("notificacion guardada");
                    io.to( user._id).emit("nueva_notificacion_r", {
                        msg: "Nueva notificacion",
                    });
                    io.to(body.postulacion).emit("nuevo_mensaje_r", {
                        msg: "Nuevo mensaje",
                    });

                })
                _saveMail(user, postulacion.usuario , link1 , postulacion.servicio)
            })
        }

    })
}
const _saveMail = (receiver, sender, link , service)=>{
    const template = "notification";
    const data ={
        firstName: receiver.nombre,
        lastName: receiver.apellido,
        link: bqConfig().appURL +  link,
        descripcion: "El usuario " + sender.nombre + " " + sender.apellido +  "ha enviado un nuevo mensaje en la  publicacion. " + service.nombre + " " + service.descripcion,
    }
    Mailer(receiver.email, "Has recibido un nuevo mensaje", null , null, template, data);
}

module.exports = mensajesPostulacionCtrl