const Categoria = require('../models/categoria');
var Bitacora = require('../models/bitacora');
const Subcategoria = require('../models/subcategoria');
const subcategoria = require('../models/subcategoria');

const categoriaCtrl = {};

categoriaCtrl.getById = (req,res)=>{
    Categoria.findById(req.params.id)
        .exec((err,categoria)=>{
            if(err){
                res.status(200).json({
                    ok:false,
                    msg:`Hubo un error al obtener la categoria con el id = ${req.params.id}`,
                    err:err
                })
            }else{
                res.status(200).json({
                    ok:true,
                    msg:'devolviendo la categoria',
                    categoria
                })
            }
        })
}


categoriaCtrl.getAll = (req,res)=>{
    Categoria.find({borrado:false})
        .exec((err,categorias)=>{
            if(err){
                res.status(200).json({
                    ok:false,
                    msg:`Hubo un error al obtener las  categorias`,
                    err:err
                })
            }else{
                res.status(200).json({
                    ok:true,
                    msg:'devolviendo las categorias',
                    categorias
                })
            }
        })
}


categoriaCtrl.getCategoriasTree = (req,res) => {
    Categoria.find({borrado:false})
    .exec((err,categorias)=>{
        if(err){
            res.status(200).json({
                ok:false,
                msg:`Hubo un error al obtener las  categorias`,
                err:err
            })
        }else{
            var categoriasTree = []
            let j = 0
            categorias.forEach((cat,i,arr) => {
               
                
            Subcategoria.find({ borrado: false, categoria: cat._id })
                    
                    .exec(async (err, subcategorias) => {  
                         await categoriasTree.push({
                            _id:cat._id,
                            nombre:cat.nombre,
                            children:subcategorias
                        })
                        // continuo aqui luego
                    j = j+1
                   
                       
                       if(j == arr.length){
                       
                        
                        res.status(200).json({
                            ok:true,
                            msg:'devolviendo las categorias',
                            categoriasTree
                        })
                           
                       }
                       
                    });

            })
           
    
            
           
        }
    })
}


categoriaCtrl.new = (req,res)=>{
    let categoria = new Categoria(req.body);
    const code =  categoria.nombre.replace(' ', '-');
    categoria.code = code.toLowerCase( );
    categoria.save((err,nuevaCategoria)=>{
        if(err){
            res.status(200).json({
                ok:false,
                msg:`Hubo un error al intentar registrar la categoria con el titulo = ${req.body.nombre}`,
                err:err
            })
        }else{
            Bitacora.create({
                usuario:req.body.idAdmin,
                accion:`Creo la categoria ${nuevaCategoria.nombre}`
            })
            res.status(200).json({
                ok:true,
                msg:'categoria registrada',
                nuevaCategoria
            })
        }
    })
}


categoriaCtrl.updateById = (req,res)=>{
    req.body.fecha_modificacion = Date.now()
    Categoria.findByIdAndUpdate(req.params.id,req.body)
        .exec((err,categoriaUpdate)=>{
            if(err){
                res.status(200).json({
                    ok:false,
                    msg:`Hubo un error al intentar modificar la categoria con el id = ${req.params.id}`,
                    err:err
                })
            }else{
                Bitacora.create({
                    usuario:req.body.idAdmin,
                    accion:`Modifico la categoria ${categoriaUpdate.nombre}`
                })
                res.status(200).json({
                    ok:true,
                    msg:'categoria modificada',
                    categoriaUpdate
                })
            }
        })
}

categoriaCtrl.updateStatusById = (req,res) => {
    Categoria.findById(req.body.id)
    .exec((err,categoria)=>{
        if(err){
            res.status(200).json({
                ok:false,
                msg:`Hubo un error al intentar cambiar el estado a la categoria`,
                err:err
            })
        }else{
            
            Categoria.findByIdAndUpdate(categoria._id,{status:!categoria.status,fecha_modificacion: Date.now()})
            .exec((err,categoria)=>{
                if(err){
                    res.status(200).json({
                        ok:false,
                        msg:`Hubo un error al intentar cambiar el estado a la categoria`,
                        err:err
                    })
                }else{
                    Bitacora.create({
                        usuario:req.body.idAdmin,
                        accion:`Cambio el estado de la categoria ${categoria.nombre} a '${!categoria.status?'Activa':'Desactivada'}'`
                    })
                    res.status(200).json({
                        ok:true,
                        msg:'categoria modificada',
                        categoria
                    })
                }
            })
        }
    })
   
}


categoriaCtrl.deleteById =  (req,res)=>{

  
                    Categoria.findByIdAndUpdate(req.params.id,{borrado:true,fecha_modificacion:Date.now()})
                    .exec((err,categoria)=>{
                        if(err){
                            res.status(200).json({
                                ok:false,
                                msg:`Hubo un error al intentar eliminar la categoria el id = ${req.params.id}`,
                                err:err
                            })
                        }else{
                            Bitacora.create({
                                usuario:req.params.idAdmin,
                                accion:`Borro la categoria ${categoria.nombre}`
                            })
                            res.status(200).json({
                                ok:true,
                                msg:'categoria eliminada',
                                categoria
                            })
                        }
                    })
                
    
}





module.exports = categoriaCtrl