const Comentario = require("../models/comentario");
var Bitacora = require("../models/bitacora");
const usersSocket = require("../userSocket");
const io = require("../socket");
const usuario = require("../models/usuario");
const respuestaComentario = require("../models/respuestaComentario");

io.on("connection", (socket) => {
  socket.on("comentario_update", () => {
    socket
      .to("administradores")
      .emit("comentario_update_r", "Comentario actualizado");
  });

  socket.on("comentario_cliente", (data) => {
    usersSocket.getUserByIdUser(data.to).then((usuario) => {
      io.to(usuario.id).emit("comentario_update_r", { msg: "cambios" });
    });
  });

  socket.on("cambios_en_datos", (data) => {
    usersSocket.getUserByIdUser(data.to).then((usuario) => {
      io.to(usuario.id).emit("cambios_en_datos_r", {});
    });
  });

  socket.on("disconnect", function () {
    usersSocket.popUserById(socket.id);
  });
});

const comentarioCtrl = {};

comentarioCtrl.getAll = async (req, res) => {
  let conditions = {
    borrado: false,
  };
  if (req.params.pendientes == "true") {
    conditions.visto = false;
  }

  Comentario.find(conditions)
    .populate("usuario")
    .sort({ fecha_creacion: -1 })
    .exec((err, comentarios) => {
      if (err) {
        res.status(200).json({
          ok: false,
          err,
        });
      } else {
        res.status(200).json({
          ok: true,
          comentarios,
          url: `${req.protocol}://${req.headers.host}/user/uploads/`,
        });
      }
    });
};
comentarioCtrl.getAllComment = async (req, res) => {
  let conditions = {
    borrado: false,
  };
 

  Comentario.find()
    .populate("usuario")
    .sort({ fecha_creacion: -1 })
    .exec((err, comentarios) => {
      if (err) {
        res.status(200).json({
          ok: false,
          err,
        });
      } else {
        res.status(200).json({
          ok: true,
          comentarios,
          url: `${req.protocol}://${req.headers.host}/user/uploads/`,
        });
      }
    });
};

comentarioCtrl.newComment = async (req, res) => {
  let comentario = new Comentario(req.body);
  comentario.save((err, comentario) => {
    if (err) {
      res.status(200).json({
        ok: false,
        err,
      });
    } else {
      Bitacora.create({
        usuario: req.body.usuario,
        accion: `Realizo un comentario en el sistema`,
      });
      res.status(200).json({
        ok: true,
        comentario,
      });
    }
  });
};
comentarioCtrl.replyComment = async (req, res) => {
  let respuesta_comentario = new respuestaComentario(req.body);
  Comentario.findByIdAndUpdate(req.params.id, {
    respuesta_comentario: respuesta_comentario,
    visto:true,
    respuesta_borrado:false
  })
    .exec((err, comentario) => {
      if (err) {
        res.status(200).json({
          ok: false,
          err,
        });
      } else {
        Bitacora.create({
          usuario: req.body.user,
          accion: `Respondido un comentario en el sistema`,
        });
        res.status(200).json({
          ok: true,
          comentario,
        });
      }
    });
};
comentarioCtrl.getByUserID = async (req, res) => {
  Comentario.find({ usuario: req.params.id, borrado: false })
    .sort({ fecha_creacion: -1 })
    .populate("usuario")
    .exec((err, comentarios) => {
      if (err) {
        res.status(200).json({
          ok: false,
          err,
        });
      } else {
        res.status(200).json({
          ok: true,
          comentarios,
          url: `${req.protocol}://${req.headers.host}/user/uploads/`,
        });
      }
    });
};

comentarioCtrl.vistoTrue = async (req, res) => {
  Comentario.findByIdAndUpdate(req.params.id, { visto: true })
    .populate("usuario")
    .exec((err, comentario) => {
      if (err) {
        res.status(200).json({
          ok: false,
          err,
        });
      } else {
        Bitacora.create({
          usuario: req.params.idAdmin,
          accion: `Marco como visto un comentario de ${comentario.usuario.username}`,
        });
        res.status(200).json({
          ok: true,
          comentario,
        });
      }
    });
};

comentarioCtrl.borrarComentario = async (req, res) => {
  Comentario.findByIdAndUpdate(req.params.id, { borrado: true })
    .populate("usuario")
    .exec((err, comentario) => {
      if (err) {
        res.status(200).json({
          ok: false,
          err,
        });
      } else {
        Bitacora.create({
          usuario: req.params.idAdmin,
          accion: `Marco como inapropiado un comentario de ${comentario.usuario.username}`,
        });
        res.status(200).json({
          ok: true,
          comentario,
        });
      }
    });
};

comentarioCtrl.borrarRespuestaComentario = async (req, res) => {
  Comentario.findByIdAndUpdate(req.params.id, { respuesta_borrado: true, visto:false} )
    .populate("usuario")
    .exec((err, comentario) => {
      if (err) {
        res.status(200).json({
          ok: false,
          err,
        });
      } else {
        Bitacora.create({
          usuario: req.params.idAdmin,
          accion: `Elimino respuesta de un comentario`,
        });
        res.status(200).json({
          ok: true,
          comentario,
        });
      }
    });
};

comentarioCtrl.borrarComentarioByUser = async (req, res) => {
  Comentario.findByIdAndUpdate(req.params.id, { borrado: true })
    .populate("usuario")
    .exec((err, comentario) => {
      if (err) {
        res.status(200).json({
          ok: false,
          err,
        });
      } else {
        Bitacora.create({
          usuario: req.params.idUser,
          accion: `Elimino un comentario del sistema`,
        });

        res.status(200).json({
          ok: true,
          comentario,
        });
      }
    });
};

comentarioCtrl.updateComentario = async (req, res) => {
  Comentario.findByIdAndUpdate(req.params.id, req.body).exec(
    (err, comentario) => {
      if (err) {
        res.status(200).json({
          ok: false,
          err,
        });
      } else {
        Bitacora.create({
          usuario: req.body.idUser,
          accion: `Edito un comentario del sistema`,
        });

        res.status(200).json({
          ok: true,
          comentario,
        });
      }
    }
  );
};
comentarioCtrl.updateRespuestaComentario = async (req, res) => {
  let respuesta_comentario = new respuestaComentario(req.body);
  Comentario.findByIdAndUpdate(req.params.id, {respuesta_comentario: respuesta_comentario}).exec(
    (err, comentario) => {
      if (err) {
        res.status(200).json({
          ok: false,
          err,
        });
      } else {
        Bitacora.create({
          usuario: req.body.user,
          accion: `Edito un comentario del sistema`,
        });

        res.status(200).json({
          ok: true,
          comentario,
        });
      }
    }
  );
};

module.exports = comentarioCtrl;
