const Cotizacion = require("../models/cotizacion");
const Servicio = require("../models/servicios");
const Postulacion = require("../models/postulaciones");
const usersSocket = require("../userSocket");
const io = require("../socket");
const cotizacionCtrl = {};

io.on("connection", (socket) => {
  console.log("se conecto", socket.id);
  socket.on("cotizacion", (data) => {
    socket.join(data.name_room);
  });

  socket.on("enviar_cotizacion", (data) => {
    io.to(`${data.name_room}`).emit("enviar_cotizacion_r");
  });

  socket.on("deshacer_cotizacion", (data) => {
    console.log(data,"1");

    io.to(`${data.name_room}`).emit("deshacer_cotizacion_r");
  });
  socket.on("editar_cotizacion", (data) => {
    console.log(data,"2");

    io.to(`${data.name_room}`).emit("editar_cotizacion_r");
  });
  socket.on("cliente_cotizacion_rechazada", (data) => {
    io.to(`${data.name_room}`).emit("cliente_cotizacion_rechazada_r");
  });
  socket.on("cliente_cotizacion_aceptada", (data) => {
    io.to(`${data.name_room}`).emit("cliente_cotizacion_aceptada_r");
  });

  socket.on("disconnect_cotizacion", (data) => {
    usersSocket.popUserById(socket.id);
    socket.leave(data.name_room);
  });
});
cotizacionCtrl.crear = async (req, res) => {
  let cotizacion = new Cotizacion(req.body);

  cotizacion.save((err, cotizacion) => {
    if (err) {
      res.status(200).json({
        ok: false,
        err,
      });
    } else {
      res.status(200).json({
        ok: true,
        cotizacion,
      });
      console.log(cotizacion._id,"3");
      console.log(req.body.servicio,"4");
    }
  });
};
cotizacionCtrl.getCotizacionById = async (req, res) => {
  Cotizacion.findOne({ _id: req.params.id }).exec((err, cotizacion) => {
    if (err) {
      res.status(200).json({
        ok: false,
        err,
      });
    } else {
      if (cotizacion != null) {
        console.log("devuelvo aqui 1");
        res.status(200).json({
          ok: true,
          cotizacion,
        });
      }
    }
  });
};
cotizacionCtrl.getCotizaciones = async (req, res) => {
  Cotizacion.find({ borrado: false }).exec((err, cotizacion) => {
    if (err) {
      res.status(200).json({
        ok: false,
        err,
      });
    } else {
      console.log("devuelvo aqui 2");
      res.status(200).json({
        ok: true,
        cotizacion,
      });
    }
  });
};
cotizacionCtrl.getCotizacionFinalized = async (req, res) => {
  if (req.body.query != "") {
    let query = req.body.query;
    or[0] = { nombre: new RegExp(query, "i") };
    or[1] = { descripcion: new RegExp(query, "i") };
  }

  Cotizacion.find({ status: 3, id_cliente: req.params.id })
    .or(or)
    .populate("servicio id_proveedor id_cliente")
    .populate({
      path: "servicio",
      populate: { path: "categoria subcategoria usuario" },
    })
    .sort({ fecha_creacion: -1 })
    .exec((err, cotizacion) => {
      if (err) {
        res.status(200).json({
          ok: false,
          err,
        });
      } else {
        if (cotizacion != null) {
          console.log("devuelvo aqui 3");
          res.status(200).json({
            ok: true,
            cotizacion,
          });
        }
      }
    });
};
cotizacionCtrl.getCotizacionByPostulacion = async (req, res) => {
  Cotizacion.findOne({ postulacion: req.params.id, status: 2 }).exec(
    (err, cotizacion) => {
      if (err) {
        res.status(200).json({
          ok: false,
          err,
        });
      } else {
        if (cotizacion != null) {
          console.log("devuelvo aqui 4");
          res.status(200).json({
            ok: true,
            cotizacion,
          });
        } else {
          Cotizacion.findOne({ postulacion: req.params.id, status: 1 }).exec(
            (err, cotizacion) => {
              if (err) {
                res.status(200).json({
                  ok: false,
                  err,
                });
              } else {
                if (cotizacion != null) {
                  console.log("devuelvo aqui 5");
                  Cotizacion.findByIdAndUpdate(req.params.id, {
                    status: 1,
                  })
                  res.status(200).json({
                    ok: true,
                    cotizacion,
                  });
                } else {
                  Cotizacion.findOne({
                    postulacion: req.params.id,
                    status: 1,
                  }).exec((err, cotizacion) => {
                    if (err) {
                      res.status(200).json({
                        ok: false,
                        err,
                      });
                    } else {
                      res.status(200).json({
                        ok: true,
                        cotizacion,
                      });
                    }
                  });
                }
              }
            }
          );
        }

        cotizacionCtrl.aceptar = async (req, res) => {
          console.log(req.body.contratoStatus,"--------------------------");

          Cotizacion.findByIdAndUpdate(req.params.id, {
            status: 1,
            contratoStatus: 1,
          })
            .populate("postulacion")
            .exec((err, cotizacion) => {
              if (err) {
                res.status(200).json({
                  ok: true,
                  cotizacion,
                });
              }
            });
        };
      }
    }
  );
};

cotizacionCtrl.aceptar = async (req, res) => {
  console.log(req.body.contratoStatus, "111111111")
  Cotizacion.findByIdAndUpdate(req.params.id, {
    status: 1,
    contratoStatus: 1,
  })
    .populate("postulacion")
    .exec((err, cotizacion) => {
      if (err) {
        res.status(200).json({
          ok: false,
          err,
        });
      } else {
        let servicio = cotizacion.servicio;
        let postulacion = cotizacion.postulacion._id;
        console.log(servicio, "----", postulacion, " xd");
        Cotizacion.updateMany(
          { servicio: cotizacion.servicio, _id: { $ne: req.params.id } },
          { $set: { status: 1 } }
        ).exec((err, cotizaciones) => {
          if (err) {
            res.status(200).json({
              ok: false,
              err,
            });
          } else {
            Servicio.findByIdAndUpdate(servicio, {
              status: 1,
              cotizacion: cotizacion._id,
            }).exec();
            Postulacion.findByIdAndUpdate(postulacion, { status: 1 }).exec();
            Postulacion.updateMany(
              { servicio: servicio, _id: { $ne: postulacion } },
              { $set: { status: 1 } }
            ).exec();
            res.status(200).json({
              ok: true,
              cotizacion,
            });
          }
        });
      }
    });
};
cotizacionCtrl.editar = async (req, res) => {
  Cotizacion.findByIdAndUpdate(req.params.id, {
    motivo: req.body.motivo,
    monto: req.body.monto,
  })
    .populate("postulacion")
    .exec((err, cotizacion) => {
      if (err) {
        res.status(200).json({
          ok: false,
          err,
        });
      } else {
        res.status(200).json({
          ok: true,
          cotizacion,
        });
      }
    });
};

cotizacionCtrl.rechazar = async (req, res) => {
  Cotizacion.findByIdAndUpdate(req.params.id, { status: 0 }).exec(
    (err, cotizacion) => {
      if (err) {
        res.status(200).json({
          ok: false,
          err,
        });
      } else {
        res.status(200).json({
          ok: true,
          cotizacion,
        });
      }
    }
  );
};

module.exports = cotizacionCtrl;
