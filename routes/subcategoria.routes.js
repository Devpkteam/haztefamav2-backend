var express = require('express');
var app = express();

var Subcategoria = require('../models/subcategoria');
const subcategoriaCtrl = require('../controller/subcategoria.controller')

app.get('/:id',(req,res)=>{
    Subcategoria.findById(req.params.id)
        .exec((err,subcategoria)=>{
            if(err){
                res.status(200).json({
                    ok:false,
                    msg:`Hubo un error al obtener la subcategoria con el id = ${req.params.id}`,
                    err:err
                })
            }else{
                res.status(200).json({
                    ok:true,
                    msg:'devolviendo la subcategoria',
                    subcategoria
                })
            }
        })
})

app.get('',(req,res)=>{
    Subcategoria.find()
        .populate('idCategoria')
        .exec((err,subcategorias)=>{
            if(err){
                res.status(200).json({
                    ok:false,
                    msg:`Hubo un error al obtener las  subcategorias`,
                    err:err
                })
            }else{
                res.status(200).json({
                    ok:true,
                    msg:'devolviendo las subcategorias',
                    subcategorias
                })
            }
        })
})


app.post('',subcategoriaCtrl.createSubcategoria)
app.post('/categoria',subcategoriaCtrl.getSubcategoriasByIdCategoria)
app.put('/:id',subcategoriaCtrl.modificarSubcategoria)
app.post('/cambiar-estatus',subcategoriaCtrl.updateStatusById)

app.post('/usuario',subcategoriaCtrl.subcategoriasByProveedor)

app.delete('/:id/:idAdmin',subcategoriaCtrl.deleteById)




module.exports = app;
