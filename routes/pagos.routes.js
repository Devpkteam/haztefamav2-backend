const express = require('express');
const app = express();

const pagosCtrl = require('../controller/pagos.controller');

app.post('/pagomembresia', pagosCtrl.pagoMembresia);

app.post('/pagoproveedor', pagosCtrl.pagoProveedor);

app.get('/historial', pagosCtrl.historial);

app.get('/historial/:id', pagosCtrl.historialByUser);

app.get('/historialId/:id', pagosCtrl.historialById);

app.get('/toExcel', pagosCtrl.toExcel);

app.get('/balance', pagosCtrl.balance);

app.post('/pagar' , pagosCtrl.pagar);


module.exports = app;