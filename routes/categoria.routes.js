var express = require('express');
var app = express();


const categoriaCtrl = require('../controller/categoria.controller')


app.get('/categoria/:id', categoriaCtrl.getById)

app.get('/categoria',categoriaCtrl.getAll)
app.get('/categoriatree',categoriaCtrl.getCategoriasTree)
// app.get('/proveedores/:id',categoriaCtrl.getProveedoresByCategoria)

app.post('',categoriaCtrl.new)

app.put('/:id',categoriaCtrl.updateById)

app.post('/cambiar-estatus',categoriaCtrl.updateStatusById)

app.delete('/:id/:idAdmin',categoriaCtrl.deleteById)




module.exports = app;
