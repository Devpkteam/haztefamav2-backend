// Requires

var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
const cors = require("cors");
const process = require('process');
const emailServiciosCola = require('./crons/serviciosEmails')
const dbModule = require('./src/module/db.module')
//Inicializar variables
const CronJob = require('cron').CronJob;
var app = express();

// CORS
app.use(cors({ origin: "*" }));
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
    res.header('Access-Control-Allow-Credentials', false);
    next();
});


// bodyParser 
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());

// Importar rutas
var def = require('./routes/default.routes.js'); 
var user = require('./routes/user');
var categoriaRoutes = require('./routes/categoria.routes');
var subcategoriaRoutes = require('./routes/subcategoria.routes');
var bitacoraRoutes = require('./routes/bitacora.routes');
var membresiasRoutes = require('./routes/membresias.routes');
var comentarioRoutes = require('./routes/comentario.routes');
var rolesRoutes = require('./routes/roles.routes');
var pagosRoutes = require('./routes/pagos.routes');
var serviciosRoutes = require('./routes/servicios.routes');
var postulacionesRoutes = require('./routes/postulaciones.routes');
var proFavRoutes = require('./routes/proveedorFav.routes');
var notificacionesRoutes = require('./routes/notificaciones.routes');
var mensajePostulacionesRoutes = require('./routes/mensajesPostulacion.routes');
var contratoRoutes = require('./routes/contrato.routes')
var paypalRoutes = require('./routes/paypal.routes')

//Deprecated
mongoose.set('useFindAndModify', false);
// Conexion a la BD
// mongoose.connect('mongodb+srv://kamila:$$Pr4y2ct48$$@cluster0.gwds7.mongodb.net/worksPk?retryWrites=true&w=majority', { useNewUrlParser: true }, (err, res) => {
mongoose.connect('mongodb://localhost:27017/worksPk', { useNewUrlParser: true }, (err, res) => {
    if (err) throw err;
    console.log('Base de datos: \x1b[32m%s\x1b[0m', 'online');
});

// Rutas

app.use('/', def);
app.use('/user', user);
app.use('/categoria',categoriaRoutes);
app.use('/subcategoria',subcategoriaRoutes);
app.use('/bitacora',bitacoraRoutes);
app.use('/membresia',membresiasRoutes);
app.use('/comentario',comentarioRoutes);
app.use('/roles', rolesRoutes);
app.use('/pagos', pagosRoutes);
app.use('/servicios', serviciosRoutes)
app.use('/postulaciones', postulacionesRoutes)
app.use('/proveedores', proFavRoutes)
app.use('/notificaciones', notificacionesRoutes)
app.use('/mensajespostulacion', mensajePostulacionesRoutes)
app.use('/cotizacion', require('./routes/cotizacion.routes'))
app.use('/contrato', contratoRoutes)
app.use('/paypal', paypalRoutes);
app.use('/calificaciones', require('./routes/calificaciones.routes'))


// Escuchar peticiones
if(process.argv.indexOf('--prod') !== -1){
    var server = app.listen(3030,'v2.corek.io', () => {
        console.log('Express server puerto 3030, --prod: \x1b[32m%s\x1b[0m', 'online');
    }); 
}else{
    var server = app.listen(3030, () => {
        console.log('Express server puerto 3030: \x1b[32m%s\x1b[0m', 'online');
    });
}
const bq360Mailing = require('./src/emails/mailing-daemon');
const cronMailing = new CronJob(
    '0 */2 * * * 1-6',
    () => {
      bq360Mailing();
    },
    null,
    true,
    'Europe/Madrid',
  );