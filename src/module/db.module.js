const config = require('../config/configurations');
// Bring Mongoose into the app
var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');

// Create the database connection
mongoose.connect(config().dbUri, {
  useUnifiedTopology: true,
  useNewUrlParser: true,
//   useMongoClient: true,
});

// CONNECTION EVENTS
// When successfully connected
mongoose.connection.on('connected', function() {
  console.info('Mongoose default connection open to ' + config().dbUri);
});

// If the connection throws an error
mongoose.connection.on('error', function(err) {
    console.error('Mongoose default connection error: ' + err);
});

// When the connection is disconnected
mongoose.connection.on('disconnected', function() {
    console.info('Mongoose default connection disconnected');
});

// If the Node process ends, close the Mongoose connection
process.on('SIGINT', function() {
  mongoose.connection.close(function() {
    console.info('Mongoose default connection disconnected through app termination');
    process.exit(0);
  });
});

// require('../server/server.schema');
require('../emails/emails.schema');
