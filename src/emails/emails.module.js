// const debug = require('debug')('BQ360::Log::Mailer');
const bqConfig = require('../config/configurations');

const mongoose = require('mongoose'),
  EmailQueue = mongoose.model('EmailQueue');

module.exports = function(to,  subject, text, html , template , data) {
  let nEmail = new EmailQueue({
    client: "PkFreelancer",
    to: to,
    subject: subject,
    template: template,
    data: data,
    text: text,
    html: html,
    from: '"' + bqConfig().smtp.from + '" <' + bqConfig().smtp.username + '>',
  });

  return nEmail.save();
};
