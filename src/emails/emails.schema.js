var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var EmailQueue = new Schema(
  {
    from: String,
    to: String,
    cc: String,
    bcc: String,
    subject: String,
    html: String,
    text: String,
    template: String,
    data: Object,
    added: { type: Date, default: Date.now },
    sended: { type: Boolean, default: false },
    discarded: Boolean,
    success: Boolean,
    client: String,
    clientOriginalID: Schema.Types.ObjectId,
    relayID: Schema.Types.ObjectId,
    sendLog: [
      {
        timestamp: Date,
        log: String,
      },
    ],
    dryRun: Boolean,
  },
  { usePushEach: true },
);

module.exports = mongoose.model('EmailQueue', EmailQueue);
