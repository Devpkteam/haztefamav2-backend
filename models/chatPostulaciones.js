var mongoose = require('mongoose');

var Schema = mongoose.Schema;


var chatPostulacionSchema = new Schema({
    usuario: { type: String, required: true , ref: 'usuario'},
    postulacion: { type: String, required: true , ref: 'postulaciones'},
    tipo: { type: String, required: true ,enum:['mensaje','imagen','documento']},
    mensaje:{type:String},
    imagen:{type:String},
    documento:{type:String},
    fecha_creacion: {type: Date, default: Date.now },
    visto:{type:Boolean, default:false},
    borrado:{type:Boolean, default:false},
    alert:{type:Boolean, default:false},
});

module.exports = mongoose.model('chatPostulacion', chatPostulacionSchema);