var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var subCategoriaSchema = new Schema({
    nombre: {type: String, required: true },
    categoria: { type: String, required: true , ref: 'categoria'},
    descripcion: {type: String},
    status: {type: Number, default: 0 },
    comision: {type: Number, default: 0 },
    borrado: {type:Boolean,default:false},
    fecha_creacion:{type:Date,default:Date.now},
    fecha_modificacion:{type:Date,default:null},
    creditos: {type: Number, required: true ,default:0} 
   

});

module.exports = mongoose.model('subcategoria', subCategoriaSchema);